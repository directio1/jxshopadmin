import axios from 'axios'
import qs from 'qs'

const postCreateAddressApi = (params) => {
	return axios
		.post(
			'http://kg.zhaodashen.cn/v1/address/create.jsp',
			qs.stringify(params)
		)
		.then((res) => res.data)
}

export default postCreateAddressApi
