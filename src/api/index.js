// import request from './request'
import axios from 'axios'
import qs from 'qs' 

import getAddressApi from './address'
import getAreaApi from './area'
import postCreateAddressApi from './createaddress'
import request from './request'
// import qs from 'qs'

axios.defaults.baseURL = 'http://kg.zhaodashen.cn/v1'

// 添加请求拦截器
axios.interceptors.request.use(function (config) {
  // 在发送请求之前做些什么  统一加 token  content-type数据编码等等
  config.headers['token'] = localStorage.getItem('token') || 'adf7cbdcdc62b07d94f86339e5687ca51'
  return config;
}, function (error) {
  // 对请求错误做些什么
  return Promise.reject(error);
});

// 添加响应拦截器 
axios.interceptors.response.use(function (response) {
  // 对响应数据做点什么   统一错误处理
  return response;
}, function (error) {
  // 对响应错误做点什么
  return Promise.reject(error);
});
 
//导出列表接口
export const getOrdersListApi = params => { //delete,token
  return axios.get('/cart/index.jsp',{params}).then(res => res.data)
}

// 收货地址
// export const getAddressApi = () => { 
//   let token = localStorage.getItem('token') || 'adf7cbdcdc62b07d94f86339e5687ca51'
//   return axios.get('/address/index.jsp', {params:{token}}).then(res => res.data);
// }
// 购物车列表
export const getCartsApi = () => { 
  let token = localStorage.getItem('token') || 'adf7cbdcdc62b07d94f86339e5687ca51'
  return axios.get('/cart/index.jsp',{params:{token}}).then(res => res.data);
}

//用户注册
export const registerApi = params => {
	return axios.post('public/reg.jsp', qs.stringify(params)).then(res => res.data)
}
// 用户名密码登录
export const loginApi = params => {
	return axios.post('public/login.jsp', qs.stringify(params)).then(res => res.data)
}
// 验证码登录
export const Securitycode = params => {
	return axios.get('public/captcha.jsp', params).then(res => res.data)
}

/**
 * 登录
 * @param {Object} params { username: '', password: ''}
 */
export function login(params) {
	return request({
		url: '/users/login',
		method: 'post',
		data: params,
	})
}

/**
 * 获取当前用户的信息
 * @param {Object} params { userid, token }
 */
export function getuserinfo(params) {
	return request({
		url: '/users/getuserinfo',
		method: 'get',
		params,
	})
}
// 列表页
export const getListApi = params => {
  return axios.get('goods/index.jsp', { params }).then(res => res.data)
}

// 商品详情接口
export const getDetailApi = params => {
  return axios.get('goods/detail.jsp', { params }).then(res => res.data)
}

// 购物车添加接口
export const postCartcreateApi = postData => {
  return axios.post('cart/create.jsp', qs.stringify(postData)).then(res => res.data)
}

export const getCartApi = () => {
  return axios.get('/cart/index.jsp', { params: { token: 'adf7cbdcdc62b07d94f86339e5687ca51' } }).then(res => res.data)
}

export { getAddressApi, getAreaApi, postCreateAddressApi }
