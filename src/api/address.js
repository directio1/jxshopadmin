import axios from 'axios'

const getAddressApi = () => {
	let token =
		localStorage.getItem('token') || 'adf7cbdcdc62b07d94f86339e5687ca51'
	return axios
		.get('http://kg.zhaodashen.cn/v1/address/index.jsp', {
			params: { token },
		})
		.then((res) => res.data)
}

export default getAddressApi
