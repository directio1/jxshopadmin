import axios from 'axios'

const getAreaApi = (params) => {
	return axios
		.get('http://kg.zhaodashen.cn/v1/area/index.jsp', { params })
		.then((res) => res.data)
}
export default getAreaApi
