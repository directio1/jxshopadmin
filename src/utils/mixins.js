import Vue from 'vue'

Vue.mixin({
    methods: {
        jump(url, id) {
            this.$router.push({
                path: url,
                query: { id: id }
            })
        }
    }
})