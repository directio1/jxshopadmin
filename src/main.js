import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

// 工具
import '@/utils/mixins.js'
// 配置全局文件&样式
import '@/utils/rem.js'  
import '@/utils/filters.js'  
import '@/assets/css/reset.scss' 

// 配置 使用vant组件库
import Vant from 'vant'
import 'vant/lib/index.css'
Vue.use(Vant);

Vue.config.productionTip = false

//导入vant库
import { Grid, GridItem } from 'vant';

Vue.use(Grid);
Vue.use(GridItem);
import 'vant/lib/index.css';


Vue.use(Vant);

new Vue({
	router,
	store,
	render: (h) => h(App),
}).$mount('#app')
