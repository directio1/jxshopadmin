import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)
import { SubmitBar } from 'vant'

Vue.use(SubmitBar)
export default new Router({
	routes: [
    {path: '/orderConfirm',component: () => import('./views/OrderConfirm.vue')},
    {path: '/orderPay',component: () => import('./views/OrderPay.vue')},
	// 登录跳转页
	{ path: '/loginjump', component: () => import('./views/login/Loginjump.vue') },
	// 注册
	{ path: '/register', component: () => import('./views/login/Register.vue') },
	// 用户名密码登录
	{ path: '/login', component: () => import('./views/login/Login.vue') },
	// 短信登录
	{ path: '/securitycode', component: () => import('./views/login/Securitycode.vue') },
	// 重置密码
	{ path: '/rest', component: () => import('./views/login/Rest.vue') },
		{ path: '/vip/star', component: () => import('./views/vip_star.vue') },
		{path:'/mine',component:() => import('./views/mine.vue')},
    {path:'/orderlist',component:() => import('./views/orderlist.vue')},
		{
			path: '/vip/address',
			component: () => import('./views/vip_address.vue'),
		},
		{
			path: '/vip/address_edit',
			component: () => import('./views/vip_address_edit.vue'),
		},
		{
			path: '/vip/address_map',
			component: () => import('./views/vip_address_map.vue'),
		},
		{ path: '/', component: () => import('./views/SY.vue') },
		{ path: '/cart', component: () => import('./views/Cart.vue') },
		{
			path: '/list',
			component: () => import('./views/list/List.vue'),
		},
		{
			path: '/goods',
			component: () => import('./views/goods/Goods.vue'),
		},
	],
})
