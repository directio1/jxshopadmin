module.exports = {
    devServer: {
        proxy: {
            '/api': {
                target: 'http://kg.zhaodashen.cn/v1/',
                ws: true,
                changeOrigin: true,
                pathRewrite: {
                    '^/api': ''
                }
            },
            // '/foo': {
            //   target: '<other_url>'
            // }
        }
    }
}